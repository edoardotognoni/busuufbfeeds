# README #

This is repo contains the code for the Android Busuu test. The test was about calling an API (of developer's choice), display the data and save it locally. There were also other non mandatory requests like compatibility,tablets layout, library use,...

I choose to get and display *Busuu's Facebook page feeds*. 

## Where did I decide to focus on? Scalability! ##

I decided to spend most of my time on builiding up a clean and scalable solution. For this I implemented the **MVP** pattern, integrating Dagger modules where possible. As you can see on the code, the core part is built completely upon Dagger. I must say also that I have only a few weeks experience with Dagger, but I tried to do my best.

Having the word *scalability* in my mind, I decided to also create all of the empty classes for a new Social Network implementation (Google+ in this case) to see how easy would be to be compatible with other platforms.  

Also, I went for the Factory pattern (implementation always with Dagger) for the DB framework. Who knows, maybe in the future we'll have a new super fast disk access DB framework. So it will be easy to integrate the new one.

### Library used ###

* Dagger - dependency injection
* gson - parsing
* Picasso - image loading and caching
* EventBus - Events handling (probably useless in this test, but since I use it many times I decided to go for it)
* ORMLite - Database framework
* Pretty time - Date formatting
* Crouton - UI alerts; Toasts suck and dialogs are bad UX in my opinion

### Testing ###

For the purpose of testing I went for **Mockito** and **Dagger**. Most of the work in testing is: Extends base Activity/Fragment providing testing dagger module in order to inject mock objects. All of the mock objects are defined inside testing dagger modules. 

**P.S.** Unfortunately I don't have much experience in TDD. All that I learnt I did it by myself in my free time. So I understand if my tests are not well designed or not complete

## Conclusions ##

I did not want to spend much time on building up a cool and fancy UI. I think that was almost out of this scope. I focused on developing a clean and scalable codebase. I know that there are still a lot of things to be done (UI/UX improvements, hard testing,...) but, as I said, I wanted to focus on the main problem: **Scalability**