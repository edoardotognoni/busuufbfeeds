package com.edoardotognoni.busuu.feeds;

import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;
import com.edoardotognoni.busuu.R;
import com.edoardotognoni.busuu.login.SocialNetwork;

/**
 * Created by opossum on 16/04/15.
 */
public class EmptyFeedsTest extends ActivityInstrumentationTestCase2<FeedsTestActivity> {
    private static final String TAG = EmptyFeedsTest.class.getSimpleName();
    private FeedsTestActivity mActivity;


    public EmptyFeedsTest() {
        super(FeedsTestActivity.class);
    }

    public void setUp() throws Exception {
        super.setUp();
        System.setProperty(
            "dexmaker.dexcache",
            getInstrumentation().getTargetContext().getCacheDir().getPath());

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.putExtra(FeedsActivity.KEY_SOCIAL_NETWORK, SocialNetwork.FACEBOOK);
        setActivityIntent(intent);

        mActivity = getActivity();
    }

    private FeedsListTestFragment startFragment() {
        SocialNetwork logged = (SocialNetwork) mActivity.getIntent().getSerializableExtra(FeedsTestActivity.KEY_SOCIAL_NETWORK);
        if (logged == null) {
            throw new IllegalArgumentException("Intent is missing logged social network instance");
        }
        FeedsListTestFragment feedsListFragment = FeedsListTestFragment.newInstance(logged, FeedsListTestFragment.TEST_TYPE.NULL_FEEDS);

        mActivity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,feedsListFragment).commit();
        getInstrumentation().waitForIdleSync();
        FeedsListTestFragment frag = (FeedsListTestFragment) mActivity.getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        return frag;
    }

    public void testFragment() {
        FeedsListTestFragment frag = startFragment();
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }
}
