package com.edoardotognoni.busuu.feeds;

import android.os.Bundle;
import com.edoardotognoni.busuu.feeds.modules.EmptyFeedsTestModule;
import com.edoardotognoni.busuu.feeds.modules.FailingParseTestModule;
import com.edoardotognoni.busuu.login.SocialNetwork;

import java.util.Arrays;
import java.util.List;

/**
 * Created by opossum on 16/04/15.
 */
public class FeedsListTestFragment extends FeedsListFragment {
    private static final String TAG = FeedsListTestFragment.class.getSimpleName();
    public enum TEST_TYPE {
        NULL_FEEDS,FAILING_PARSE
    }

    @Override
    public List<Object> getModules() {
        TEST_TYPE test_type = (TEST_TYPE) getArguments().getSerializable("test_type");
        switch (test_type) {
            case NULL_FEEDS:
                return Arrays.<Object>asList(new EmptyFeedsTestModule(this));
            case FAILING_PARSE:
                return Arrays.<Object>asList(new FailingParseTestModule(this));
            default:
                return Arrays.<Object>asList(new EmptyFeedsTestModule(this));
        }
    }

    public static FeedsListTestFragment newInstance(SocialNetwork socialLogged,TEST_TYPE test_type) {
        FeedsListTestFragment fragment = new FeedsListTestFragment();

        Bundle args = new Bundle();
        args.putSerializable("test_type",test_type);
        args.putSerializable(KEY_SOCIAL_NETWORK, socialLogged);
        fragment.setArguments(args);

        return fragment;
    }
}
