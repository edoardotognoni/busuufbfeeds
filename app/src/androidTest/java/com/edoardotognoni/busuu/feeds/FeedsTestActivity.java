package com.edoardotognoni.busuu.feeds;

import android.os.Bundle;
import com.edoardotognoni.busuu.BaseActivity;
import com.edoardotognoni.busuu.R;
import com.edoardotognoni.busuu.feed_detail.FeedDetailFragment;
import de.greenrobot.event.EventBus;

import java.util.Arrays;
import java.util.List;

/**
 * Created by opossum on 16/04/15.
 */
public class FeedsTestActivity extends BaseActivity {
    private static final String TAG = FeedsTestActivity.class.getSimpleName();
    public static final String KEY_SOCIAL_NETWORK = "key_social_network";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feeds);
    }

    @Override
    public List<Object> getModules() {
        return Arrays.<Object>asList();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEvent(EventShowDetailFragment eventShowDetailFragment) {
        FeedDetailFragment feedDetailFragment = FeedDetailFragment.newInstance(eventShowDetailFragment.getFeed());
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,feedDetailFragment).addToBackStack(null).commit();
    }

    public static class EventShowDetailFragment {
        private SocialFeed mFeed;

        public EventShowDetailFragment(SocialFeed feed) {
            mFeed = feed;
        }

        public SocialFeed getFeed() {
            return mFeed;
        }
    }
}
