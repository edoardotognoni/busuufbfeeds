package com.edoardotognoni.busuu.feeds.modules;

import com.edoardotognoni.busuu.feeds.FeedsLoadedCallback;
import com.edoardotognoni.busuu.feeds.interactor.FacebookFeedsInteractor;
import com.edoardotognoni.busuu.feeds.interactor.FeedsInteractor;
import com.edoardotognoni.busuu.feeds.interactor.GoogleFeedsInteractor;
import com.edoardotognoni.busuu.feeds.interactor.parsers.FacebookFeedParser;
import dagger.Module;
import dagger.Provides;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import javax.inject.Named;

import static junit.framework.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@Module(
    library = true
)
public class EmptyFeedsInteractorTestModule {

    @Provides
    FacebookFeedParser providesFacebookFeedParser() {
        FacebookFeedParser parser = spy(new FacebookFeedParser());
        when(parser.parseFeed(null)).thenReturn(null);
        assertNull(parser.parseFeed(null));
        return parser;
    }

    @Provides
    @Named("fb feeds interactor")
    public FeedsInteractor providesFacebookFeedsInteractor(FacebookFeedParser parser) {
        FacebookFeedsInteractor fbFeedsInteractor = spy(new FacebookFeedsInteractor(parser));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                ((FeedsLoadedCallback) invocationOnMock.getArguments()[0]).onFeedsLoaded(null,null);
                return null;
            }
        }).when(fbFeedsInteractor).getPosts(any(FeedsLoadedCallback.class));
        return fbFeedsInteractor;
    }

    @Provides
    @Named("google feeds interactor")
    public FeedsInteractor providesGoogleFeedsInteractor() {
        return new GoogleFeedsInteractor();
    }
}