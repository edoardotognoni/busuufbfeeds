package com.edoardotognoni.busuu.feeds.modules;

import android.app.Application;
import com.edoardotognoni.busuu.BusuuAppModule;
import com.edoardotognoni.busuu.db.ORMLiteDatabaseManager;
import com.edoardotognoni.busuu.db.FactoryDatabaseInterface;
import com.edoardotognoni.busuu.feeds.FeedsListTestFragment;
import com.edoardotognoni.busuu.feeds.FeedsView;
import com.edoardotognoni.busuu.feeds.manager.FeedsManager;
import com.edoardotognoni.busuu.feeds.presenter.FeedsPresenter;
import com.edoardotognoni.busuu.feeds.presenter.FeedsPresenterImpl;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

@Module(
    injects = FeedsListTestFragment.class,
    addsTo = BusuuAppModule.class,
    includes = EmptyFeedsInteractorTestModule.class,
    complete = false
)
public class EmptyFeedsTestModule {
    private static final String TAG = EmptyFeedsTestModule.class.getSimpleName();
    private FeedsView mFeedsView;

    public EmptyFeedsTestModule(FeedsView feedsView) {
        mFeedsView = feedsView;
    }

    @Provides
    public FeedsView provideView() {
        return mFeedsView;
    }

    @Provides
    public FeedsPresenter providePresenter(FeedsView feedsView, FeedsManager feedsManager) {
        return new FeedsPresenterImpl(feedsView, feedsManager);
    }

    @Provides
    @Singleton
    public FactoryDatabaseInterface providesDatabaseManager(Application contextApplication) {
        ORMLiteDatabaseManager dbSpy = spy(new ORMLiteDatabaseManager(contextApplication));
        doReturn(null).when(dbSpy).getAllSocialFeeds();
        return dbSpy;
    }

}