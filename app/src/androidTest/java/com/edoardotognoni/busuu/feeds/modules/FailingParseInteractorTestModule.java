package com.edoardotognoni.busuu.feeds.modules;

import com.edoardotognoni.busuu.feeds.interactor.FacebookFeedsInteractor;
import com.edoardotognoni.busuu.feeds.interactor.FeedsInteractor;
import com.edoardotognoni.busuu.feeds.interactor.GoogleFeedsInteractor;
import com.edoardotognoni.busuu.feeds.interactor.parsers.FacebookFeedParser;
import com.google.gson.JsonSyntaxException;
import dagger.Module;
import dagger.Provides;
import org.json.JSONObject;

import javax.inject.Named;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

@Module(
    library = true
)
public class FailingParseInteractorTestModule {

    @Provides
    FacebookFeedParser providesFacebookFeedParser() {
        FacebookFeedParser parser = spy(new FacebookFeedParser());
        when(parser.parseFeed(any(JSONObject.class))).thenThrow(new JsonSyntaxException("Can't parse data"));
        return parser;
    }

    @Provides
    @Named("fb feeds interactor")
    public FeedsInteractor providesFacebookFeedsInteractor(FacebookFeedParser parser) {
        return new FacebookFeedsInteractor(parser);
    }

    @Provides
    @Named("google feeds interactor")
    public FeedsInteractor providesGoogleFeedsInteractor() {
        return new GoogleFeedsInteractor();
    }
}