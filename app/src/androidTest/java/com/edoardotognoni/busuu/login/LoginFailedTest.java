package com.edoardotognoni.busuu.login;

import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import com.edoardotognoni.busuu.R;

/**
 * Created by opossum on 16/04/15.
 */
public class LoginFailedTest extends ActivityInstrumentationTestCase2<LoginTestActivity> {
    private static final String TAG = LoginFailedTest.class.getSimpleName();
    private LoginTestActivity mLoginActivity;
    private Button mLogin;

    public LoginFailedTest() {
        super(LoginTestActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        System.setProperty(
            "dexmaker.dexcache",
            getInstrumentation().getTargetContext().getCacheDir().getPath());

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.putExtra(LoginTestActivity.KEY_LOGIN_RESULT,LoginTestActivity.LOGIN_RESULT.ERROR);
        setActivityIntent(intent);
        mLoginActivity = getActivity();
        mLogin = (Button) mLoginActivity.findViewById(R.id.facebook_login_button);
    }

    public void testLogin() {
        mLoginActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mLogin.performClick();
            }
        });
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}
