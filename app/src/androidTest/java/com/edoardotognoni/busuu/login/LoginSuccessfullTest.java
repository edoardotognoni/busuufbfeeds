package com.edoardotognoni.busuu.login;

import android.app.Instrumentation;
import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import com.edoardotognoni.busuu.R;
import com.edoardotognoni.busuu.feeds.FeedsActivity;

/**
 * Created by opossum on 16/04/15.
 */
public class LoginSuccessfullTest extends ActivityInstrumentationTestCase2<LoginTestActivity> {
    private static final String TAG = LoginSuccessfullTest.class.getSimpleName();
    private LoginTestActivity mLoginActivity;
    private Button mLogin;

    public LoginSuccessfullTest() {
        super(LoginTestActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        System.setProperty(
            "dexmaker.dexcache",
            getInstrumentation().getTargetContext().getCacheDir().getPath());

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.putExtra(LoginTestActivity.KEY_LOGIN_RESULT,LoginTestActivity.LOGIN_RESULT.SUCCESS);
        setActivityIntent(intent);
        mLoginActivity = getActivity();
        mLogin = (Button) mLoginActivity.findViewById(R.id.facebook_login_button);
    }

    public void testLogin() {
        mLoginActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mLogin.performClick();
            }
        });

        //Check if it opens FeedActivity
        // register next activity that need to be monitored.
        Instrumentation.ActivityMonitor activityMonitor = getInstrumentation().addMonitor(FeedsActivity.class.getName(), null, false);
        FeedsActivity nextActivity = (FeedsActivity) getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 5000);
        // next activity is opened and captured.
        assertNotNull(nextActivity);
        nextActivity.finish();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}
