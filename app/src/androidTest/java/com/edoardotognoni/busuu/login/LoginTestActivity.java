package com.edoardotognoni.busuu.login;

import android.content.Intent;
import com.edoardotognoni.busuu.login.modules.LoginCanceledTestModule;
import com.edoardotognoni.busuu.login.modules.LoginErrorTestModule;
import com.edoardotognoni.busuu.login.modules.LoginSuccessTestModule;

import java.util.Arrays;
import java.util.List;

/**
 * Created by opossum on 16/04/15.
 */
public class LoginTestActivity extends LoginActivity {
    private static final String TAG = LoginTestActivity.class.getSimpleName();
    //Key for test Intent.
    public static final String KEY_LOGIN_RESULT = "key_result";

    public enum LOGIN_RESULT {
        SUCCESS,CANCELED,ERROR
    }

    @Override
    public List<Object> getModules() {
        Intent intent = getIntent();
        LOGIN_RESULT loginResult = (LOGIN_RESULT) intent.getSerializableExtra(KEY_LOGIN_RESULT);
        switch (loginResult) {
            case SUCCESS:
                return Arrays.<Object>asList(new LoginSuccessTestModule(this));
            case CANCELED:
                return Arrays.<Object>asList(new LoginCanceledTestModule(this));
            case ERROR:
                return Arrays.<Object>asList(new LoginErrorTestModule(this));
            default:
                return Arrays.<Object>asList(new LoginSuccessTestModule(this));
        }
    }

}
