package com.edoardotognoni.busuu.login.modules;

import android.app.Activity;
import com.edoardotognoni.busuu.login.interactors.FacebookLoginInteractorImpl;
import com.edoardotognoni.busuu.login.interactors.GoogleLoginInteractorImpl;
import com.edoardotognoni.busuu.login.interactors.LoginCallbackListener;
import com.edoardotognoni.busuu.login.interactors.LoginInteractor;
import com.facebook.CallbackManager;
import dagger.Module;
import dagger.Provides;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import javax.inject.Named;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;

@Module(
    library = true
)
public class InteractorCanceledTestModule {

    @Provides
    public CallbackManager providesFbCallbackManager() {
        return CallbackManager.Factory.create();
    }

    @Provides
    @Named("fb login interactor")
    public LoginInteractor providesFacebookLoginInteractor(final CallbackManager callbackManager) {
        FacebookLoginInteractorImpl loginInteractor = mock(FacebookLoginInteractorImpl.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                ((LoginCallbackListener) invocationOnMock.getArguments()[1]).onLoginCanceled();
                return null;
            }
        }).when(loginInteractor).login(any(Activity.class), any(LoginCallbackListener.class));
        return loginInteractor;
    }

    @Provides
    @Named("google login interactor")
    public LoginInteractor providesGoogleLoginInteractor() {
        return new GoogleLoginInteractorImpl();
    }
}