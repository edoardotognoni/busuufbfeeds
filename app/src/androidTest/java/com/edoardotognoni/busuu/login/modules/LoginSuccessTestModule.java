package com.edoardotognoni.busuu.login.modules;

import com.edoardotognoni.busuu.BusuuAppModule;
import com.edoardotognoni.busuu.login.LoginTestActivity;
import com.edoardotognoni.busuu.login.LoginView;
import com.edoardotognoni.busuu.login.SocialNetwork;
import com.edoardotognoni.busuu.login.interactors.SocialLoginInteractorManager;
import com.edoardotognoni.busuu.login.presenter.LoginPresenter;
import com.edoardotognoni.busuu.login.presenter.LoginPresenterImpl;
import dagger.Module;
import dagger.Provides;

import static org.mockito.Mockito.*;

@Module(
    injects = LoginTestActivity.class,
    includes = InteractorSuccessTestModule.class,
    addsTo = BusuuAppModule.class
)
public class LoginSuccessTestModule {
    private LoginView mLoginView;


    public LoginSuccessTestModule(LoginView loginView) {
        mLoginView = loginView;
    }

    @Provides
    public LoginView provideView() {
        return mLoginView;
    }


    @Provides
    public LoginPresenter providePresenter(LoginView loginView, SocialLoginInteractorManager socialLoginInteractorManager) {
        SocialLoginInteractorManager manager = spy(socialLoginInteractorManager);
        when(manager.isLogged()).thenReturn(false);
        doReturn(SocialNetwork.FACEBOOK).when(manager).getSocialNetworkLogged();
        return new LoginPresenterImpl(loginView,manager);
    }

}