package com.edoardotognoni.busuu;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import dagger.ObjectGraph;

import java.util.List;

/**
 * Activity that provides base access for Dagger modules.
 */
public abstract class BaseActivity extends ActionBarActivity {
    private static final String TAG = BaseActivity.class.getSimpleName();
    private ObjectGraph mActivityGraph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mActivityGraph = ((BusuuApp) getApplication()).createScopedModules(getModules().toArray());
        if (!getModules().isEmpty()) {
            mActivityGraph.inject(this);
        }
        super.onCreate(savedInstanceState);
    }

    public ObjectGraph createScopedModules(Object... modules) {
        return mActivityGraph.plus(modules);
    }

    /**
     * Dagger modules. If not module is needed in Activity implementation, simply pass an empty List
     * @return Dagger modules to be added to the base graph
     */
    public abstract List<Object> getModules();


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivityGraph = null;
    }

}
