package com.edoardotognoni.busuu;

import android.app.Activity;
import android.support.v4.app.Fragment;
import dagger.ObjectGraph;

import java.util.List;

/**
 * Base fragment who proides accessibility for adding new Dagger modules.
 * It gets BaseActivity objectGraph and adds the provided modules
 */
public abstract class BaseFragment extends Fragment {
    private static final String TAG = BaseFragment.class.getSimpleName();
    private ObjectGraph mObjectGraph;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // expand the activity graph with the fragment-specific module(s)
        mObjectGraph = ((BaseActivity) activity).createScopedModules(getModules().toArray());
        mObjectGraph.inject(this);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mObjectGraph = null;
    }

    /**
     * Scoped Dagger modules
     * @return List of modules
     */
    public abstract List<Object> getModules();

}
