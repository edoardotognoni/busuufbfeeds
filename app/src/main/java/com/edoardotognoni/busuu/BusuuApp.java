package com.edoardotognoni.busuu;

import android.app.Application;
import com.facebook.FacebookSdk;
import dagger.ObjectGraph;

import java.util.Arrays;
import java.util.List;

public class BusuuApp extends Application {
    private static final String TAG = BusuuApp.class.getSimpleName();
    private ObjectGraph objectGraph;

    @Override
    public void onCreate() {
        super.onCreate();
        objectGraph = ObjectGraph.create(getModules().toArray());
        FacebookSdk.sdkInitialize(getApplicationContext());
    }

    private List<Object> getModules() {
        return Arrays.<Object>asList(new BusuuAppModule(this));
    }

    /**
     * Add modules to the base objectGraph
     * @param modules Dagger modules
     * @return ObjectGraph
     */
    public ObjectGraph createScopedModules(Object... modules) {
        return objectGraph.plus(modules);
    }
}
