package com.edoardotognoni.busuu;

import android.app.Application;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

/**
 * Base module taht provides Application object (Application context)
 */
@Module(
    library = true
)
public class BusuuAppModule {
    private static final String TAG = BusuuAppModule.class.getSimpleName();

    private final BusuuApp mBusuuApp;

    public BusuuAppModule(BusuuApp busuuApp) {
        this.mBusuuApp = busuuApp;
    }

    @Provides
    @Singleton
    Application provideApplicationContext() {
        return mBusuuApp;
    }
}
