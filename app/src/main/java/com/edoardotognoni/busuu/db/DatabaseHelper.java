package com.edoardotognoni.busuu.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.edoardotognoni.busuu.feeds.SocialFeed;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.List;

/**
 * True ORMLite database class.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final String TAG = DatabaseHelper.class.getSimpleName();
    // name of the database file
    private static final String DATABASE_NAME = "busuufeeds.sqlite";
    private Context mContext;
    /** This is used to identify DB version. When DB needs to be changed, this must be increased in order for {@link #onUpgrade(SQLiteDatabase, int, int)} to be called */
    private static final int DATABASE_VERSION = 1;
    private Dao<SocialFeed, String> mFeedsDao = null;

    protected DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

    /**
     * Initializes DB. Get's called at first creation of the DB
     * @param database
     * @param connectionSource
     */
    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTableIfNotExists(connectionSource, SocialFeed.class);
        }
        catch (java.sql.SQLException e) {
            throw new RuntimeException("Database can't be created!", e);
        }

    }



    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        if (oldVersion < 2) {
            //Do all the changes that has been implemented on version 2 of the DB
        }

    }

    public Dao<SocialFeed, String> getFeedsDao() {
        if (null == mFeedsDao) {
            try {
                mFeedsDao = getDao(SocialFeed.class);
            }
            catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return mFeedsDao;
    }

    public void saveFeeds(List<SocialFeed> feeds) {
        for (SocialFeed feed : feeds) {
            try {
                getFeedsDao().createOrUpdate(feed);
            }
            catch (SQLException e) {
                e.printStackTrace();
                Log.w(TAG,"Can't save feed. Skipping");
            }
        }
    }

    public List<SocialFeed> getFeeds() {
        try {
            return getFeedsDao().queryForAll();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}

