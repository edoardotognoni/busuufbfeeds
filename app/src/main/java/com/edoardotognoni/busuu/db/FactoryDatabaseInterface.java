package com.edoardotognoni.busuu.db;

import com.edoardotognoni.busuu.feeds.SocialFeed;

import java.util.List;

/**
 * Factory interface for the DB framework.
 */
public interface FactoryDatabaseInterface {

    List<SocialFeed> getAllSocialFeeds();

    void saveSocialFeeds(final List<SocialFeed> feeds);
}
