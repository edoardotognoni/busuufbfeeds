package com.edoardotognoni.busuu.db;

import android.content.Context;
import android.util.Log;
import com.edoardotognoni.busuu.feeds.SocialFeed;

import java.util.List;

/**
 * ORMLite Database wrapper.
 */
public class ORMLiteDatabaseManager implements FactoryDatabaseInterface{
    private static final String TAG = ORMLiteDatabaseManager.class.getSimpleName();

    private Context mCtx;
    private DatabaseHelper helper;

    public ORMLiteDatabaseManager(Context ctx) {
        mCtx = ctx;
        helper = new DatabaseHelper(ctx);
    }

    @Override
    public List<SocialFeed> getAllSocialFeeds() {
        return helper.getFeeds();
    }

    @Override
    public void saveSocialFeeds(final List<SocialFeed> feeds) {
        // Span a new thread because it could be a long operation.
        //We don't want to block UI thread
        new Thread(new Runnable() {
            @Override
            public void run() {
                helper.saveFeeds(feeds);
                Log.d(TAG,"Feeds saved");
            }
        }).start();

    }
}
