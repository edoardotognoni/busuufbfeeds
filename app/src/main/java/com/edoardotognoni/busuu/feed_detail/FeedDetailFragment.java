package com.edoardotognoni.busuu.feed_detail;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.edoardotognoni.busuu.R;
import com.edoardotognoni.busuu.feeds.SocialFeed;
import com.squareup.picasso.Picasso;

public class FeedDetailFragment extends Fragment {
    private static final String TAG = FeedDetailFragment.class.getSimpleName();
    public static final String KEY_FEED = "key_feed";

    public static FeedDetailFragment newInstance(SocialFeed socialFeed) {
        FeedDetailFragment fragment = new FeedDetailFragment();

        Bundle args = new Bundle();
        args.putParcelable(KEY_FEED, socialFeed);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_feed_detail,container, false);
        if (getArguments() == null) {
            throw new UnsupportedOperationException("Must pass arguments to the FeedDetailFragment");
        }

        if (getArguments().getParcelable(KEY_FEED) == null) {
            throw new UnsupportedOperationException("Must pass the feed to the FeedDetailFragment");
        }

        ImageView fromPicture = (ImageView) root.findViewById(R.id.feed_detail_from_picture);
        TextView fromName = (TextView) root.findViewById(R.id.feed_detail_from_name);
        ImageView feedImage = (ImageView) root.findViewById(R.id.feed_image);
        TextView feedDetailMessage = (TextView) root.findViewById(R.id.feed_detail_message);

        SocialFeed feed = getArguments().getParcelable(KEY_FEED);

        fromName.setText(feed.getFromName());
        Picasso.with(getActivity()).load(feed.getFromProfilePic()).placeholder(R.drawable.user_no_pic).into(fromPicture);
        feedDetailMessage.setText(feed.getFeedMessage());
        Picasso.with(getActivity()).load(feed.getImagelink()).into(feedImage);
        return root;
    }
}
