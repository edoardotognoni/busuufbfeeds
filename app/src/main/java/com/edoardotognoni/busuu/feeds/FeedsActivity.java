package com.edoardotognoni.busuu.feeds;

import android.os.Bundle;
import com.edoardotognoni.busuu.BaseActivity;
import com.edoardotognoni.busuu.R;
import com.edoardotognoni.busuu.feed_detail.FeedDetailFragment;
import com.edoardotognoni.busuu.login.SocialNetwork;
import de.greenrobot.event.EventBus;
import de.keyboardsurfer.android.widget.crouton.Crouton;

import java.util.Arrays;
import java.util.List;

public class FeedsActivity extends BaseActivity {
    private static final String TAG = FeedsActivity.class.getSimpleName();
    public static final String KEY_SOCIAL_NETWORK = "key_social_network";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feeds);

        if (savedInstanceState == null) {
            SocialNetwork logged = (SocialNetwork) getIntent().getSerializableExtra(KEY_SOCIAL_NETWORK);
            if (logged == null) {
                throw new IllegalArgumentException("Intent is missing logged social network instance");
            }
            FeedsListFragment feedsListFragment = FeedsListFragment.newInstance(logged);

            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,feedsListFragment).commit();
        }
    }

    @Override
    public List<Object> getModules() {
        return Arrays.<Object>asList();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Crouton.cancelAllCroutons();
    }

    public void onEvent(EventShowDetailFragment eventShowDetailFragment) {
        FeedDetailFragment feedDetailFragment = FeedDetailFragment.newInstance(eventShowDetailFragment.getFeed());
        //If we are on tablets...
        if (findViewById(R.id.fragment_detail_container) != null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_detail_container,feedDetailFragment).commit();
        }
        else {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, feedDetailFragment).addToBackStack(
                null).commit();
        }
    }

    public static class EventShowDetailFragment {
        private SocialFeed mFeed;

        public EventShowDetailFragment(SocialFeed feed) {
            mFeed = feed;
        }

        public SocialFeed getFeed() {
            return mFeed;
        }
    }
}
