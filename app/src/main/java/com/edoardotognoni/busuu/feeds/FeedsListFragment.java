package com.edoardotognoni.busuu.feeds;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.edoardotognoni.busuu.BaseFragment;
import com.edoardotognoni.busuu.R;
import com.edoardotognoni.busuu.feeds.module.FeedsModule;
import com.edoardotognoni.busuu.feeds.presenter.FeedsPresenter;
import com.edoardotognoni.busuu.login.SocialNetwork;
import com.squareup.picasso.Picasso;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import org.ocpsoft.pretty.time.PrettyTime;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

public class FeedsListFragment extends BaseFragment implements FeedsView {
    private static final String TAG = FeedsListFragment.class.getSimpleName();
    public static final String KEY_SOCIAL_NETWORK = "key_social_network";

    @Inject
    FeedsPresenter mFeedsPresenter;

    private ListView mFeedsListView;
    private ProgressBar mFeedsLoadingBar;


    public static FeedsListFragment newInstance(SocialNetwork socialLogged) {
        FeedsListFragment fragment = new FeedsListFragment();

        Bundle args = new Bundle();
        args.putSerializable(KEY_SOCIAL_NETWORK, socialLogged);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_feeds,container, false);
        mFeedsListView = (ListView) root.findViewById(R.id.feeds_list_view);
        mFeedsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SocialFeed feed = (SocialFeed) parent.getItemAtPosition(position);
                mFeedsPresenter.onFeedClicked(feed);
            }
        });
        mFeedsLoadingBar = (ProgressBar) root.findViewById(R.id.feeds_loading_progress_bar);
        mFeedsPresenter.init(getArguments());
        return root;
    }

    @Override
    public List<Object> getModules() {
        return Arrays.<Object>asList(new FeedsModule(this));
    }

    @Override
    public void showLoadingBar() {
        mFeedsLoadingBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingBar() {
        mFeedsLoadingBar.setVisibility(View.GONE);
    }

    @Override
    public void feedsLoaded(List<SocialFeed> feeds, FeedsSource source) {
        mFeedsLoadingBar.setVisibility(View.GONE);
        //If local feeds are showing, call notifyDatasetChanged
        //We care about good UX
        if (mFeedsListView.getAdapter() != null) {
            FeedsViewAdapter adapter = (FeedsViewAdapter) mFeedsListView.getAdapter();
            adapter.setFeeds(feeds);
            adapter.notifyDataSetChanged();
        }
        else {
            mFeedsListView.setAdapter(new FeedsViewAdapter(feeds));
        }
    }

    @Override
    public void onError(FeedsRequestErrorType feedsRequestErrorType) {
        mFeedsLoadingBar.setVisibility(View.GONE);
        if (isAdded()) {
            int errorString = 0;
            switch (feedsRequestErrorType) {
                case NETWORK:
                    errorString = R.string.network_error;
                    break;
                case PARSING:
                    errorString = R.string.parsing_error;
                    break;
            }
            Crouton.makeText(getActivity(),errorString, Style.ALERT).show();
        }
    }

    private static class ViewHolder {
        ImageView fromImage;
        TextView fromName;
        TextView feedDate;
        TextView feedMessage;
    }

    private class FeedsViewAdapter extends BaseAdapter {
        private List<SocialFeed> mFeeds;
        private final PrettyTime mPrettyTime;

        public FeedsViewAdapter(List<SocialFeed> feeds) {
            mFeeds = feeds;
            mPrettyTime = new PrettyTime();
        }

        @Override
        public int getCount() {
            return mFeeds.size();
        }

        @Override
        public Object getItem(int position) {
            return mFeeds.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.adapter_feed_list, parent, false);
                holder.fromImage = (ImageView) convertView.findViewById(R.id.from_picture);
                holder.fromName = (TextView) convertView.findViewById(R.id.feed_from_name);
                holder.feedDate = (TextView) convertView.findViewById(R.id.feed_date);
                holder.feedMessage = (TextView) convertView.findViewById(R.id.feed_message);
                convertView.setTag(holder);
            }
            else {
                holder = (ViewHolder) convertView.getTag();
            }

            SocialFeed feed = mFeeds.get(position);
            holder.fromName.setText(feed.getFromName());
            holder.feedDate.setText(mPrettyTime.format(feed.getCreationDate()));
            holder.feedMessage.setText(feed.getFeedMessage());
            Picasso.with(getActivity()).load(feed.getFromProfilePic()).placeholder(R.drawable.user_no_pic).into(holder.fromImage);

            return convertView;
        }

        public List<SocialFeed> getFeeds() {
            return mFeeds;
        }

        public void setFeeds(List<SocialFeed> feeds) {
            mFeeds = feeds;
        }
    }
}
