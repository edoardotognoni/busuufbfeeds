package com.edoardotognoni.busuu.feeds;

import java.util.List;

/**
 * Callback for feeds retrieving.
 */
public interface FeedsLoadedCallback {

    void onFeedsLoaded(List<SocialFeed> feeds, FeedsSource source);

    /**
     * For some social network, high res images must be retrieved with another call to their API. This callback is
     * used for that.
     * @param feed
     * @param image
     */
    void onHighResImageLoaded(SocialFeed feed, String image);

    void onErrorRetrieveingFeeds(FeedsRequestErrorType feedsRequestErrorType);
}
