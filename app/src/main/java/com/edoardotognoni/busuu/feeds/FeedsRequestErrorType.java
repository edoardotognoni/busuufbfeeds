package com.edoardotognoni.busuu.feeds;

/**
 * Feeds retrieving errors.
 */
public enum FeedsRequestErrorType {
    NETWORK,PARSING
}
