package com.edoardotognoni.busuu.feeds;

/**
 * Enumeration that tells if feeds have been retrieved locally or from network
 */
public enum FeedsSource {
    LOCAL,NETWORK
}
