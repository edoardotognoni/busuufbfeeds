package com.edoardotognoni.busuu.feeds;

import java.util.List;

/**
 * MVP view for feeds page
 */
public interface FeedsView {

    void showLoadingBar();

    void hideLoadingBar();

    void feedsLoaded(List<SocialFeed> feeds, FeedsSource source);

    void onError(FeedsRequestErrorType feedsRequestErrorType);
}
