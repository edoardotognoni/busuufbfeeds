package com.edoardotognoni.busuu.feeds;

import android.os.Parcel;
import android.os.Parcelable;
import com.edoardotognoni.busuu.login.SocialNetwork;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Feed wrapper for each social network post.
 */
@DatabaseTable
public class SocialFeed implements Parcelable,Comparable<SocialFeed> {
    @DatabaseField(id = true)
    private String id;
    @DatabaseField
    private SocialNetwork mSocialNetwork;
    @DatabaseField
    private FEED_TYPE mType;
    @DatabaseField
    private String imagelink;
    @DatabaseField
    private String objectId;
    @DatabaseField
    private String feedMessage;
    @DatabaseField
    private Date creationDate;
    @DatabaseField
    private String mFromName;
    @DatabaseField
    private String mFromProfilePic;
    @DatabaseField
    private String mLink;

    //Needed
    public SocialFeed() {

    }

    public String getImagelink() {
        return imagelink;
    }

    public void setImagelink(String imagelink) {
        this.imagelink = imagelink;
    }

    public String getFeedMessage() {
        return feedMessage;
    }

    public void setFeedMessage(String feedMessage) {
        this.feedMessage = feedMessage;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getFromName() {
        return mFromName;
    }

    public void setFromName(String fromName) {
        mFromName = fromName;
    }

    public String getFromProfilePic() {
        return mFromProfilePic;
    }

    public void setFromProfilePic(String fromProfilePic) {
        mFromProfilePic = fromProfilePic;
    }

    public FEED_TYPE getType() {
        return mType;
    }

    public void setType(FEED_TYPE type) {
        mType = type;
    }

    public String getLink() {
        return mLink;
    }

    public void setLink(String link) {
        mLink = link;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public SocialNetwork getSocialNetwork() {
        return mSocialNetwork;
    }

    public void setSocialNetwork(SocialNetwork socialNetwork) {
        mSocialNetwork = socialNetwork;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int compareTo(SocialFeed another) {
        return getCreationDate().compareTo(another.getCreationDate()) * -1;
    }

    public enum FEED_TYPE {
        PICTURE,LINK,MESSAGE
    }

    protected SocialFeed(Parcel in) {
        id = in.readString();
        mType = (FEED_TYPE) in.readSerializable();
        imagelink = in.readString();
        feedMessage = in.readString();
        long tmpCreationDate = in.readLong();
        creationDate = tmpCreationDate != -1 ? new Date(tmpCreationDate) : null;
        mFromName = in.readString();
        mFromProfilePic = in.readString();
        mLink = in.readString();
        objectId = in.readString();
        mSocialNetwork = (SocialNetwork) in.readSerializable();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeSerializable(mType);
        dest.writeString(imagelink);
        dest.writeString(feedMessage);
        dest.writeLong(creationDate != null ? creationDate.getTime() : -1L);
        dest.writeString(mFromName);
        dest.writeString(mFromProfilePic);
        dest.writeString(mLink);
        dest.writeString(objectId);
        dest.writeSerializable(mSocialNetwork);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<SocialFeed> CREATOR = new Parcelable.Creator<SocialFeed>() {
        @Override
        public SocialFeed createFromParcel(Parcel in) {
            return new SocialFeed(in);
        }

        @Override
        public SocialFeed[] newArray(int size) {
            return new SocialFeed[size];
        }
    };
}
