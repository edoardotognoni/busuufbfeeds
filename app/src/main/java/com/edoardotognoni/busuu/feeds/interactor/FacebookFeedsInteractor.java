package com.edoardotognoni.busuu.feeds.interactor;

import android.os.Bundle;
import android.util.Log;
import com.edoardotognoni.busuu.feeds.FeedsLoadedCallback;
import com.edoardotognoni.busuu.feeds.FeedsRequestErrorType;
import com.edoardotognoni.busuu.feeds.FeedsSource;
import com.edoardotognoni.busuu.feeds.SocialFeed;
import com.edoardotognoni.busuu.feeds.interactor.parsers.FeedParser;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Facebook feeds interactor
 */
public class FacebookFeedsInteractor implements FeedsInteractor {
    private static final String TAG = FacebookFeedsInteractor.class.getSimpleName();

    private FeedParser mFeedParser;

    public FacebookFeedsInteractor(FeedParser feedParser) {
        mFeedParser = feedParser;
    }

    @Override
    public void getPosts(final FeedsLoadedCallback callback) {
        GraphRequest.Callback reqCallback = new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                if (graphResponse.getError() != null) {
                    Log.e(TAG, "Error receiveing FB Feeds: " + graphResponse.getError());
                    callback.onErrorRetrieveingFeeds(FeedsRequestErrorType.NETWORK);
                    return;
                }
                List<SocialFeed> feeds = null;
                try {
                    feeds = parseFeedReponse(graphResponse.getJSONObject());
                    callback.onFeedsLoaded(feeds, FeedsSource.NETWORK);
                }
                catch (JSONException e) {
                    Log.e(TAG, "Can't parse response from facebook: " + e);
                    callback.onErrorRetrieveingFeeds(FeedsRequestErrorType.PARSING);
                }
            }
        };

        Bundle parameters = new Bundle();
        parameters.putString("fields",
                             "feed.limit(100).fields(object_id,status_type,link,picture,message,type,description,from," +
                             "caption)");

        GraphRequest request = new GraphRequest(AccessToken.getCurrentAccessToken(),
                                                "/busuucom",
                                                parameters, HttpMethod.GET, reqCallback);
        request.executeAsync();
    }

    @Override
    public void getHighResImage(final SocialFeed feed, final FeedsLoadedCallback callback) {
        GraphRequest.Callback reqCallback = new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                String highRes = "";
                if (graphResponse.getError() != null) {
                    Log.w(TAG, "Error getting high res image " + graphResponse.getError());
                }
                else {
                    try {
                        highRes = parseHighResImageResponse(graphResponse.getJSONObject());
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                        Log.w(TAG, "Can't parse high res image response. Nevermind, let's keep low res one");
                    }
                }
                callback.onHighResImageLoaded(feed, highRes);
            }
        };

        Bundle parameters = new Bundle();
        parameters.putString("fields","images");

        GraphRequest request = new GraphRequest(AccessToken.getCurrentAccessToken(),
                                                "/" + feed.getObjectId(),
                                                parameters, HttpMethod.GET, reqCallback);
        request.executeAsync();
    }

    private String parseHighResImageResponse(JSONObject object) throws JSONException {
        if (object == null) {
            return null;
        }
        JSONArray data = object.getJSONArray("images");
        if (data.length() > 0) {
            //Let's take the first one. Resolution management is out of scope
            JSONObject image = data.getJSONObject(0);
            return image.getString("source");
        }
        return null;
    }

    private List<SocialFeed> parseFeedReponse(JSONObject jsonObject) throws JSONException {
        JSONObject feedData = jsonObject.getJSONObject("feed");
        JSONArray data = feedData.getJSONArray("data");
        List<SocialFeed> feeds = new ArrayList<>(data.length());
        for (int i = 0; i < data.length(); i++) {
            JSONObject feed = data.getJSONObject(i);
            SocialFeed feedBean = mFeedParser.parseFeed(feed);
            if (feedBean != null) {
                feeds.add(feedBean);
            }
        }
        return feeds;
    }
}
