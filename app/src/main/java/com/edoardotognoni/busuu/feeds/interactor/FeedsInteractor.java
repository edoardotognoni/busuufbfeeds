package com.edoardotognoni.busuu.feeds.interactor;

import com.edoardotognoni.busuu.feeds.FeedsLoadedCallback;
import com.edoardotognoni.busuu.feeds.SocialFeed;

/**
 * Basic interface for feeds interactors
 */
public interface FeedsInteractor {

    /**
     * Get posts from the net and parse them. Results are passed to the {@link FeedsLoadedCallback}
     * @param callback
     */
    void getPosts(FeedsLoadedCallback callback);

    /**
     * Get high res image for the feed. The feed must have its object_id set to
     * correctly retrieve the high res image. For implementations: If you don't need to retrieve high res images, simply call
     * {@link FeedsLoadedCallback#onHighResImageLoaded(SocialFeed, String)} passing the same SocialFeed and the URL image string
     * @param feed
     * @param callback
     */
    void getHighResImage(SocialFeed feed, FeedsLoadedCallback callback);
}
