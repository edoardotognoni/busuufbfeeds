package com.edoardotognoni.busuu.feeds.interactor;

import com.edoardotognoni.busuu.feeds.interactor.parsers.FacebookFeedParser;
import dagger.Module;
import dagger.Provides;

import javax.inject.Named;

/**
 * Dagger module for feeds interactors
 */
@Module(
    library = true
)
public class FeedsInteractorModule {

    @Provides
    @Named("fb feeds interactor")
    public FeedsInteractor providesFacebookFeedsInteractor() {
        return new FacebookFeedsInteractor(new FacebookFeedParser());
    }

    @Provides
    @Named("google feeds interactor")
    public FeedsInteractor providesGoogleFeedsInteractor() {
        return new GoogleFeedsInteractor();
    }
}
