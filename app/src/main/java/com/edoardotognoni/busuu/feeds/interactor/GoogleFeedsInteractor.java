package com.edoardotognoni.busuu.feeds.interactor;

import com.edoardotognoni.busuu.feeds.FeedsLoadedCallback;
import com.edoardotognoni.busuu.feeds.SocialFeed;

/**
 * Google feeds interactor
 */
public class GoogleFeedsInteractor implements FeedsInteractor {

    @Override
    public void getPosts(FeedsLoadedCallback callback) {

    }

    @Override
    public void getHighResImage(SocialFeed feed, FeedsLoadedCallback callback) {
    }
}
