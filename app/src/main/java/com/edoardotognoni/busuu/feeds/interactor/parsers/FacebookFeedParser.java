package com.edoardotognoni.busuu.feeds.interactor.parsers;

import android.util.Log;
import com.edoardotognoni.busuu.feeds.SocialFeed;
import com.edoardotognoni.busuu.login.SocialNetwork;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Facebook feed parser
 */
public class FacebookFeedParser implements FeedParser {
    private static final String TAG = FacebookFeedParser.class.getSimpleName();
    private final Gson mGson;
    private static final String FB_DATE_FORMAT ="yyyy-MM-dd'T'hh:mm:ss";
    SimpleDateFormat fbDateFormat = new SimpleDateFormat(FB_DATE_FORMAT);
    public static final String FACEBOOK_GRAPH_URL = "https://graph.facebook.com/";
    public static final String FACEBOOK_PROFILE_PICTURE_URL = FACEBOOK_GRAPH_URL + "%s/picture?type=large";

    public FacebookFeedParser() {
        final GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(SocialFeed.class, new FBSocialFeedParseAdapter());
        mGson = gsonBuilder.create();
    }

    @Override
    public SocialFeed parseFeed(JSONObject feed)  {
        if (feed == null) {
            return null;
        }
        try {
            return mGson.fromJson(feed.toString(), SocialFeed.class);
        }
        catch (JsonSyntaxException exception) {
            Log.e(TAG,"Can't parse feed: " + exception);
            return null;
        }
    }


    /**
     * Type adapter for gson parsing
     */
    private class FBSocialFeedParseAdapter extends TypeAdapter<SocialFeed> {

        @Override
        public SocialFeed read(final JsonReader in) throws IOException {
            final SocialFeed myClassInstance = new SocialFeed();
            myClassInstance.setSocialNetwork(SocialNetwork.FACEBOOK);

            in.beginObject();
            while (in.hasNext()) {
                String jsonTag = in.nextName();
                if ("type".equals(jsonTag)) {
                    String feedType = in.nextString();
                    if ("status".equals(feedType)) {
                        myClassInstance.setType(SocialFeed.FEED_TYPE.MESSAGE);
                    }
                    else if ("photo".equals(feedType)) {
                        myClassInstance.setType(SocialFeed.FEED_TYPE.PICTURE);
                    }
                    else if ("link".equals(feedType)) {
                        myClassInstance.setType(SocialFeed.FEED_TYPE.LINK);
                    }
                    else {
                        Log.d(TAG, "Unknown feed type: " + feedType);
                    }
                }
                else if ("id".equals(jsonTag)) {
                    myClassInstance.setId(in.nextString());
                }
                else if ("message".equals(jsonTag)) {
                    myClassInstance.setFeedMessage(in.nextString());
                }
                else if ("picture".equals(jsonTag)) {
                    myClassInstance.setImagelink(in.nextString());
                }
                else if ("link".equals(jsonTag)) {
                    myClassInstance.setLink(in.nextString());
                }
                else if ("object_id".equals(jsonTag)) {
                    //This is to get high res images
                    myClassInstance.setObjectId(in.nextString());
                }
                else if ("created_time".equals(jsonTag)) {
                    try {
                        myClassInstance.setCreationDate(fbDateFormat.parse(in.nextString()));
                    }
                    catch (ParseException e) {
                        Log.e(TAG,"Can't parse FB feed date: " + e);
                    }
                }
                else if ("from".equals(jsonTag)) {
                    in.beginObject();
                    while (in.hasNext()) {
                        String innerJsonTag = in.nextName();
                        if ("id".equals(innerJsonTag)) {
                            String id = in.nextString();
                            myClassInstance.setFromProfilePic(String.format(FACEBOOK_PROFILE_PICTURE_URL, id));
                        }
                        else if ("name".equals(innerJsonTag)) {
                            myClassInstance.setFromName(in.nextString());
                        }
                        else {
                            //We don't care about this field
                            in.skipValue();
                        }
                    }
                    in.endObject();
                }
                else {
                    //We don't care about this field
                    in.skipValue();
                }
            }
            in.endObject();

            return myClassInstance;
        }

        @Override
        public void write(final JsonWriter out, final SocialFeed myClassInstance)
            throws IOException {
            out.beginObject();
            // Out of test scope for now
            out.endObject();
        }

    }
}
