package com.edoardotognoni.busuu.feeds.interactor.parsers;

import com.edoardotognoni.busuu.feeds.SocialFeed;
import org.json.JSONObject;

/**
 * Basic interface for feeds parsers
 */
public interface FeedParser {

    SocialFeed parseFeed(JSONObject jsonFeed);
}
