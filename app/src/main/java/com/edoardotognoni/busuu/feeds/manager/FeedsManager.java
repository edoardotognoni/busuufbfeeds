package com.edoardotognoni.busuu.feeds.manager;

import com.edoardotognoni.busuu.db.FactoryDatabaseInterface;
import com.edoardotognoni.busuu.feeds.FeedsLoadedCallback;
import com.edoardotognoni.busuu.feeds.FeedsSource;
import com.edoardotognoni.busuu.feeds.SocialFeed;
import com.edoardotognoni.busuu.feeds.interactor.FeedsInteractor;
import com.edoardotognoni.busuu.login.SocialNetwork;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Manager that routes calls to the right social network interactor. It is also responsible to retrieve
 * local feeds and get them back to the presenter
 */
public class FeedsManager {

    @Inject
    @Named("fb feeds interactor")
    FeedsInteractor mFbFeedsInteractor;

    @Inject
    @Named("google feeds interactor")
    FeedsInteractor mGoogleFeedsInteractor;

    @Inject
    FactoryDatabaseInterface mDatabaseManager;

    public void getPosts(SocialNetwork socialNetwork,FeedsLoadedCallback callback) {
        switch (socialNetwork) {
            case FACEBOOK:
                mFbFeedsInteractor.getPosts(callback);
                break;
            case GOOGLE:
                mGoogleFeedsInteractor.getPosts(callback);
                break;
        }

        //While we wait for the network call, load local feeds if any
        List<SocialFeed> feeds = mDatabaseManager.getAllSocialFeeds();
        //If we have no feeds in DB, we wait for the network ones
        if (feeds != null && !feeds.isEmpty()) {
            callback.onFeedsLoaded(feeds, FeedsSource.LOCAL);
        }
    }

    public void saveFeeds(List<SocialFeed> feeds) {
        mDatabaseManager.saveSocialFeeds(feeds);
    }

    public void retrieveHighResImage(SocialFeed socialFeed, FeedsLoadedCallback callback) {
        switch (socialFeed.getSocialNetwork()) {
            case FACEBOOK:
                mFbFeedsInteractor.getHighResImage(socialFeed,callback);
                break;
            case GOOGLE:
                mGoogleFeedsInteractor.getHighResImage(socialFeed,callback);
                break;
        }
    }
}
