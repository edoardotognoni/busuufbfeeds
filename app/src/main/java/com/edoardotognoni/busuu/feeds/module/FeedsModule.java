package com.edoardotognoni.busuu.feeds.module;

import android.app.Application;
import com.edoardotognoni.busuu.BusuuAppModule;
import com.edoardotognoni.busuu.db.ORMLiteDatabaseManager;
import com.edoardotognoni.busuu.db.FactoryDatabaseInterface;
import com.edoardotognoni.busuu.feeds.FeedsListFragment;
import com.edoardotognoni.busuu.feeds.FeedsView;
import com.edoardotognoni.busuu.feeds.interactor.FeedsInteractorModule;
import com.edoardotognoni.busuu.feeds.manager.FeedsManager;
import com.edoardotognoni.busuu.feeds.presenter.FeedsPresenter;
import com.edoardotognoni.busuu.feeds.presenter.FeedsPresenterImpl;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

/**
 * Dagger module for feeds presenter
 */
@Module(
    injects = FeedsListFragment.class,
    addsTo = BusuuAppModule.class,
    includes = FeedsInteractorModule.class,
    complete = false
)
public class FeedsModule {
    private static final String TAG = FeedsModule.class.getSimpleName();
    private FeedsView mFeedsView;

    public FeedsModule(FeedsView feedsView) {
        mFeedsView = feedsView;
    }

    @Provides
    public FeedsView provideView() {
        return mFeedsView;
    }

    @Provides
    public FeedsPresenter providePresenter(FeedsView feedsView, FeedsManager feedsManager) {
        return new FeedsPresenterImpl(feedsView, feedsManager);
    }

    /**
     * ORMLite DB for now
     * @param contextApplication
     * @return
     */
    @Provides
    @Singleton
    public FactoryDatabaseInterface providesDatabaseManager(Application contextApplication) {
        return new ORMLiteDatabaseManager(contextApplication);
    }

}
