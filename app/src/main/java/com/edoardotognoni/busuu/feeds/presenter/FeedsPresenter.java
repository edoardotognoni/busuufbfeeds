package com.edoardotognoni.busuu.feeds.presenter;

import android.os.Bundle;
import com.edoardotognoni.busuu.feeds.SocialFeed;

/**
 * Basic interface for feeds presenters
 */
public interface FeedsPresenter {

    /**
     * Called when fragment is created. Retrieve posts locally (if any) and meanwhile make the API call to get the new ones
     * @param arguments
     */
    void init(Bundle arguments);

    void onFeedClicked(SocialFeed feed);
}
