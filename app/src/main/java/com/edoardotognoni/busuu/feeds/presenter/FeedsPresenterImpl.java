package com.edoardotognoni.busuu.feeds.presenter;

import android.os.Bundle;
import android.text.TextUtils;
import com.edoardotognoni.busuu.feeds.*;
import com.edoardotognoni.busuu.feeds.manager.FeedsManager;
import com.edoardotognoni.busuu.login.SocialNetwork;
import de.greenrobot.event.EventBus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Presenter for MVP feeds
 */
public class FeedsPresenterImpl implements FeedsPresenter,FeedsLoadedCallback {
    private static final String TAG = FeedsPresenterImpl.class.getSimpleName();
    private FeedsView mFeedsView;
    private FeedsManager mFeedsManager;


    public FeedsPresenterImpl(FeedsView feedsView, FeedsManager feedsManager) {
        mFeedsView = feedsView;
        mFeedsManager = feedsManager;
    }

    @Override
    public void init(Bundle arguments) {
        if (arguments == null) {
            throw new IllegalArgumentException("Missing argument inside FeedsListFragment");
        }
        SocialNetwork logged = (SocialNetwork) arguments.getSerializable(FeedsListFragment.KEY_SOCIAL_NETWORK);
        if (logged == null) {
            throw new IllegalArgumentException("Missing argument inside FeedsListFragment");
        }
        mFeedsView.showLoadingBar();
        mFeedsManager.getPosts(logged, this);
    }

    @Override
    public void onFeedClicked(SocialFeed feed) {
        if (SocialFeed.FEED_TYPE.PICTURE.equals(feed.getType())) {
            mFeedsView.showLoadingBar();
            mFeedsManager.retrieveHighResImage(feed,this);
        }
        else {
            EventBus.getDefault().post(new FeedsActivity.EventShowDetailFragment(feed));
        }
    }

    @Override
    public void onFeedsLoaded(List<SocialFeed> feeds, FeedsSource source) {
        if (feeds == null) {
            feeds = new ArrayList<>();
        }
        if (FeedsSource.NETWORK.equals(source)) {
            mFeedsManager.saveFeeds(feeds);
        }
        Collections.sort(feeds);
        mFeedsView.feedsLoaded(feeds, source);
    }

    @Override
    public void onHighResImageLoaded(SocialFeed feed, String image) {
        mFeedsView.hideLoadingBar();
        //If image is empty, it means there have been some problems.
        //Nevermind, keep the old one
        if (!TextUtils.isEmpty(image)) {
            feed.setImagelink(image);
        }
        EventBus.getDefault().post(new FeedsActivity.EventShowDetailFragment(feed));
    }

    @Override
    public void onErrorRetrieveingFeeds(FeedsRequestErrorType feedsRequestErrorType) {
        mFeedsView.onError(feedsRequestErrorType);
    }
}
