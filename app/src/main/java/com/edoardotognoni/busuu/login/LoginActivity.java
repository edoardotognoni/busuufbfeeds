package com.edoardotognoni.busuu.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.edoardotognoni.busuu.BaseActivity;
import com.edoardotognoni.busuu.R;
import com.edoardotognoni.busuu.feeds.FeedsActivity;
import com.edoardotognoni.busuu.login.module.LoginModule;
import com.edoardotognoni.busuu.login.presenter.LoginPresenter;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

public class LoginActivity extends BaseActivity implements LoginView {
    private static final String TAG = LoginActivity.class.getSimpleName();
    @Inject
    LoginPresenter mLoginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mLoginPresenter.init();
        Button loginButton = (Button) findViewById(R.id.facebook_login_button);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLoginPresenter.login(LoginActivity.this, SocialNetwork.FACEBOOK);
            }
        });
    }

    @Override
    public List<Object> getModules() {
        return Arrays.<Object>asList(new LoginModule(this));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mLoginPresenter.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void goToFeedsSection(SocialNetwork socialNetwork) {
        Intent intent = new Intent(this, FeedsActivity.class);
        intent.putExtra(FeedsActivity.KEY_SOCIAL_NETWORK, socialNetwork);
        startActivity(intent);
        finish();
    }

    @Override
    public void onLoginError(String error) {
        Crouton.makeText(this,getString(R.string.unable_to_login, error), Style.ALERT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Crouton.cancelAllCroutons();
    }
}
