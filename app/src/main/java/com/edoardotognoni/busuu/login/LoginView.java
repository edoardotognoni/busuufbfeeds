package com.edoardotognoni.busuu.login;

/**
 * View for the MVP in LoginActivity
 */
public interface LoginView {

    void goToFeedsSection(SocialNetwork socialNetwork);

    void onLoginError(String message);
}
