package com.edoardotognoni.busuu.login;

public enum SocialNetwork {
    FACEBOOK, GOOGLE
}
