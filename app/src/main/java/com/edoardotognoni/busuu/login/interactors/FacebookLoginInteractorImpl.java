package com.edoardotognoni.busuu.login.interactors;

import android.app.Activity;
import android.content.Intent;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.util.Arrays;

/**
 * Facebook login interactor
 */
public class FacebookLoginInteractorImpl implements LoginInteractor {
    private static final String TAG = FacebookLoginInteractorImpl.class.getSimpleName();
    private final CallbackManager mCallbackManager;

    public FacebookLoginInteractorImpl(CallbackManager callbackManager) {
        mCallbackManager = callbackManager;
    }

    @Override
    public void login(Activity activity, final LoginCallbackListener listener) {
        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                listener.onLoginSuccessfull();
            }

            @Override
            public void onCancel() {
                listener.onLoginCanceled();
            }

            @Override
            public void onError(FacebookException exception) {
                listener.onLoginError(exception);
            }
        });
        LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.<String>asList());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean isLogged() {
        AccessToken token = AccessToken.getCurrentAccessToken();
        if (token != null) {
            if (!token.isExpired()) {
                return true;
            }
        }
        return false;
    }
}
