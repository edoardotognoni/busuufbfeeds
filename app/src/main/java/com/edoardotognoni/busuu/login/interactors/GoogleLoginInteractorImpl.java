package com.edoardotognoni.busuu.login.interactors;

import android.app.Activity;
import android.content.Intent;

/**
 * Google login interactors
 */
public class GoogleLoginInteractorImpl implements LoginInteractor {
    private static final String TAG = GoogleLoginInteractorImpl.class.getSimpleName();

    @Override
    public void login(Activity activity, LoginCallbackListener listener) {
        //To be implemented
        throw new UnsupportedOperationException("This feature has still to be implemented");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //To be implemented
    }

    @Override
    public boolean isLogged() {
        //To be implemented
        return false;
    }
}
