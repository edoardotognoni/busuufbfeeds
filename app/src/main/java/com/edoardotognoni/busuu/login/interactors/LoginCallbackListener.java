package com.edoardotognoni.busuu.login.interactors;

/**
 * Listener used to get login callbacks
 */
public interface LoginCallbackListener {

    void onLoginSuccessfull();

    void onLoginError(Exception e);

    void onLoginCanceled();

}
