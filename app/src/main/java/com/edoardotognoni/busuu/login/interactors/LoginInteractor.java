package com.edoardotognoni.busuu.login.interactors;

import android.app.Activity;
import android.content.Intent;

/**
 * Basic interface for login interactors
 */
public interface LoginInteractor {

    /**
     * Performs login. Results are passed to the {@link LoginCallbackListener}
     * @param activity
     * @param listener
     */
    void login(Activity activity, LoginCallbackListener listener);

    void onActivityResult(int requestCode, int resultCode, Intent data);

    boolean isLogged();
}
