package com.edoardotognoni.busuu.login.interactors;

import com.facebook.CallbackManager;
import dagger.Module;
import dagger.Provides;

import javax.inject.Named;

/**
 * Dagger module to provide login interactors
 */
@Module(
    library = true
)
public class LoginInteractorModule {
    private static final String TAG = LoginInteractorModule.class.getSimpleName();

    @Provides
    public CallbackManager providesFbCallbackManager() {
        return CallbackManager.Factory.create();
    }

    @Provides
    @Named("fb login interactor")
    public LoginInteractor providesFacebookLoginInteractor(CallbackManager callbackManager) {
        return new FacebookLoginInteractorImpl(callbackManager);
    }

    @Provides
    @Named("google login interactor")
    public LoginInteractor providesGoogleLoginInteractor() {
        return new GoogleLoginInteractorImpl();
    }
}
