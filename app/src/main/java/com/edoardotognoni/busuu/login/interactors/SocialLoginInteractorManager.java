package com.edoardotognoni.busuu.login.interactors;

import android.app.Activity;
import android.content.Intent;
import com.edoardotognoni.busuu.login.SocialNetwork;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Manager who routes calls to the right interactors (Facebook,Google,...)
 */
public class SocialLoginInteractorManager {
    private static final String TAG = SocialLoginInteractorManager.class.getSimpleName();

    @Inject
    @Named("fb login interactor")
    LoginInteractor mFbInteractor;
    @Inject
    @Named("google login interactor")
    LoginInteractor mGoogleInteractor;

    private SocialNetwork askedLogin;

    /**
     * Performs login based on the {@link SocialNetwork} provided
     * @param activity
     * @param listener
     * @param socialNetwork
     */
    public void login(Activity activity, LoginCallbackListener listener, SocialNetwork socialNetwork) {
        switch (socialNetwork) {
            case FACEBOOK:
                askedLogin = SocialNetwork.FACEBOOK;
                mFbInteractor.login(activity, listener);
                break;
            case GOOGLE:
                askedLogin = SocialNetwork.GOOGLE;
                mGoogleInteractor.login(activity,listener);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (askedLogin) {
            case FACEBOOK:
                mFbInteractor.onActivityResult(requestCode, resultCode, data);
                break;
            case GOOGLE:
                mGoogleInteractor.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    /**
     * Check for all the interactors to see if one of them is logged. I chosed
     * to check FB first.
     * @return true if logged, false otherwise
     */
    public boolean isLogged() {
        //I choose to check FB first
        if (mFbInteractor.isLogged()) {
            return true;
        }
        else if (mGoogleInteractor.isLogged()){
            return true;
        }
        return false;
    }

    /**
     * Returns the {@link SocialNetwork} logged
     * @return
     */
    public SocialNetwork getSocialNetworkLogged() {
        //I choose to check FB first
        if (mFbInteractor.isLogged()) {
            return SocialNetwork.FACEBOOK;
        }
        else if (mGoogleInteractor.isLogged()){
            return SocialNetwork.GOOGLE;
        }
        return null;
    }
}
