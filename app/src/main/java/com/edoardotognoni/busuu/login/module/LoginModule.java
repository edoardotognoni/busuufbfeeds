package com.edoardotognoni.busuu.login.module;

import com.edoardotognoni.busuu.BusuuAppModule;
import com.edoardotognoni.busuu.login.LoginActivity;
import com.edoardotognoni.busuu.login.LoginView;
import com.edoardotognoni.busuu.login.interactors.LoginInteractorModule;
import com.edoardotognoni.busuu.login.interactors.SocialLoginInteractorManager;
import com.edoardotognoni.busuu.login.presenter.LoginPresenter;
import com.edoardotognoni.busuu.login.presenter.LoginPresenterImpl;
import dagger.Module;
import dagger.Provides;

/**
 * Dagger login module
 */
@Module(
    injects = LoginActivity.class,
    includes = LoginInteractorModule.class,
    addsTo = BusuuAppModule.class
)
public class LoginModule {
    private static final String TAG = LoginModule.class.getSimpleName();
    private LoginView mLoginView;


    public LoginModule(LoginView loginView) {
        mLoginView = loginView;
    }

    @Provides
    public LoginView provideView() {
        return mLoginView;
    }

    @Provides
    public LoginPresenter providePresenter(LoginView loginView,SocialLoginInteractorManager socialLoginInteractorManager) {
        return new LoginPresenterImpl(loginView, socialLoginInteractorManager);
    }

}
