package com.edoardotognoni.busuu.login.presenter;

import android.app.Activity;
import android.content.Intent;
import com.edoardotognoni.busuu.login.SocialNetwork;

/**
 * Presenter interface for MVP
 */
public interface LoginPresenter {

    /**
     * Performs login with the provided {@link SocialNetwork}
     * @param activity
     * @param socialNetwork
     */
    void login(Activity activity, SocialNetwork socialNetwork);

    /**
     * Must be called when the Activity first launches. It checks if we are already logged or not. If so, route
     * the user to the feeds page
     */
    void init();

    void onActivityResult(int requestCode, int resultCode, Intent data);
}
