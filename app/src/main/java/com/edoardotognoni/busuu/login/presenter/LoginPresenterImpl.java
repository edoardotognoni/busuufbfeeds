package com.edoardotognoni.busuu.login.presenter;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import com.edoardotognoni.busuu.login.LoginView;
import com.edoardotognoni.busuu.login.SocialNetwork;
import com.edoardotognoni.busuu.login.interactors.LoginCallbackListener;
import com.edoardotognoni.busuu.login.interactors.SocialLoginInteractorManager;

/**
 * Presenter for the login module
 */
public class LoginPresenterImpl implements LoginPresenter, LoginCallbackListener {
    private static final String TAG = LoginPresenterImpl.class.getSimpleName();
    private LoginView mLoginView;
    SocialLoginInteractorManager mSocialLoginInteractorManager;


    public LoginPresenterImpl(LoginView loginView, SocialLoginInteractorManager socialLoginInteractorManager) {
        mLoginView = loginView;
        mSocialLoginInteractorManager = socialLoginInteractorManager;
    }

    @Override
    public void login(Activity activity, SocialNetwork socialNetwork) {
        mSocialLoginInteractorManager.login(activity,this,socialNetwork);
    }

    @Override
    public void onLoginSuccessfull() {
        Log.d(TAG, "Login successfull");
        SocialNetwork socialNetwork = mSocialLoginInteractorManager.getSocialNetworkLogged();
        mLoginView.goToFeedsSection(socialNetwork);
    }

    @Override
    public void onLoginError(Exception e) {
        Log.w(TAG,"Login error: " + e);
        mLoginView.onLoginError(e.getLocalizedMessage());
    }

    @Override
    public void onLoginCanceled() {
        Log.d(TAG,"Login canceled");
    }

    @Override
    public void init() {
        //If we are already logged, skip to the feeds page
        if (mSocialLoginInteractorManager.isLogged() && mSocialLoginInteractorManager.getSocialNetworkLogged() != null) {
            //Get to the next section
            SocialNetwork logged = mSocialLoginInteractorManager.getSocialNetworkLogged();
            mLoginView.goToFeedsSection(logged);
        }
        else {
            //Show login button
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mSocialLoginInteractorManager != null) {
            mSocialLoginInteractorManager.onActivityResult(requestCode, resultCode, data);
        }
    }
}
